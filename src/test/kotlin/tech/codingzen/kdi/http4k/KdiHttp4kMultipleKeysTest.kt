package tech.codingzen.kdi.http4k

import junit.framework.TestCase.assertEquals
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.http4k.core.*
import org.http4k.core.Status.Companion.OK
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.http4k.server.Undertow
import org.http4k.server.asServer
import org.junit.Test
import tech.codingzen.kdi.data_structure.Kdi
import tech.codingzen.kdi.data_structure.ScopeExecutor
import tech.codingzen.kdi.dsl.ScopeDsl
import tech.codingzen.kdi.logging.Logger
import tech.codingzen.kdi.spec.BaseSpec
import tech.codingzen.kdi.spec.MultipleSpec

data class TestKey1(val data: Int)
data class TestKey2(val data: Int)

class KdiHttp4kMultipleKeysTest {
  @Test
  fun name() = runBlocking {
    Kdi.appSpec()
      .printLogger(Logger.Level.TRACE)
      .noScopeExtensions()
      .spec(
        MultipleSpec(Http4kSetupScope, ServerScope)
      )
      .execute {
        val key1 = it.http4k.getKey<TestKey1>()
        val key2 = it.http4k.getKey<TestKey2>()

        val app = it.http4k.initialiseRequestContext()
          .then(routes("/health" bind Method.GET to { request ->
            request.with(key1 of TestKey1(10), key2 of TestKey2(20))
            Response(Status.OK).body("${key1(request)} ${key2(request)}")
          }))

        val test = app(Request(Method.GET, "/health"))
        assertEquals(test.body.toString(), "TestKey1(data=10) TestKey2(data=20)")
      }
  }

  object ServerScope : BaseSpec.Delegate {
    override val scopeSpec = Kdi.scopeSpec("server")
      .noBootstrapper()
      .module {
        http4k.requiredKey<TestKey1>()
        http4k.requiredKey<TestKey2>()
      }
      .build()
  }
}