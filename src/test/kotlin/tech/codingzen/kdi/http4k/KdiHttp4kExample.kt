//package tech.codingzen.kdi.http4k
//
//import kotlinx.coroutines.runBlocking
//import org.http4k.core.*
//import org.http4k.core.Method.GET
//import org.http4k.core.Status.Companion.OK
//import org.http4k.lens.Header
//import org.http4k.lens.int
//import org.http4k.routing.bind
//import org.http4k.routing.routes
//import org.http4k.server.Undertow
//import org.http4k.server.asServer
//import tech.codingzen.kdi.dsl.Kdi
//import tech.codingzen.kdi.dsl.builder.scope.BuiltScope
//import tech.codingzen.kdi.dsl.execute
//import tech.codingzen.kdi.dsl.scopes
//import tech.codingzen.kdi.dsl.then
//import tech.codingzen.kdi.http4k.IRequestContextKey.Companion.defaultedKey
//import tech.codingzen.kdi.logging.Logger
//
//fun main(args: Array<String>) = runBlocking {
//  Kdi.rootScopeBuilder
//    .printLogger(Logger.Level.TRACE)
//    .noBootstrapper()
//    .noScopeExtensions()
//    .build()
//    .execute(
//      scopes()
//        .then(Http4kSetupScope)
//        .then(ServerScope)
//        .then(ServerScopeExecutor)
//    )
//}
//
//object ServerScope : BuiltScope.Delegate {
//  override val delegate: BuiltScope = Kdi.scopeBuilder("server")
//    .inheritLogger()
//    .noBootstrapper()
//    .module {
//      http4k.key(::VersionHdrKey)
//      single { VersionHdrFilter(get()) }
//
//      single { CreateHandler(get()) }
//
//      http4k.server {
//        val routes = routes(
//          "/health" bind GET to { Response(OK) },
//          "/car" bind routes(
//            Method.POST to get<CreateHandler>()
//          )
//        )
//        val app = http4k.initialiseRequestContext().then(get<VersionHdrFilter>()).then(routes)
//        app.asServer(Undertow(8080))
//      }
//    }
//    .build()
//}
//
//class VersionHdrKey(contexts: RequestContexts) : IRequestContextKey<Int> by defaultedKey(contexts, -1)
//class VersionHdrFilter(private val versionHdr: VersionHdrKey) : Filter {
//  companion object {
//    val lens = Header.int().optional("version")
//  }
//
//  override fun invoke(next: HttpHandler): HttpHandler = { request ->
//    lens(request)?.let { request.with(versionHdr of it) } ?: request
//    next(request)
//  }
//}
//
//class CreateHandler(private val versionHdrKey: VersionHdrKey) : HttpHandler {
//  override fun invoke(request: Request): Response {
//    return Response(OK).body("version: ${versionHdrKey(request)}")
//  }
//}