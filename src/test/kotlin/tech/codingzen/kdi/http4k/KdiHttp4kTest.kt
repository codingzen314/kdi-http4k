package tech.codingzen.kdi.http4k

import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.http4k.core.Method
import org.http4k.core.Response
import org.http4k.core.Status.Companion.OK
import org.http4k.core.then
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.http4k.server.Undertow
import org.http4k.server.asServer
import org.junit.Test
import tech.codingzen.kdi.data_structure.Kdi
import tech.codingzen.kdi.data_structure.ScopeExecutor
import tech.codingzen.kdi.dsl.ScopeDsl
import tech.codingzen.kdi.logging.Logger
import tech.codingzen.kdi.spec.BaseSpec
import tech.codingzen.kdi.spec.MultipleSpec

class KdiHttp4kTest {
  @Test
  fun name() = runBlocking {
    Kdi.appSpec()
      .printLogger(Logger.Level.TRACE)
      .noScopeExtensions()
      .spec(
        MultipleSpec(Http4kSetupScope, ServerScope)
      )
      .execute(TestScopeExecutor)
  }

  object ServerScope : BaseSpec.Delegate {
    override val scopeSpec = Kdi.scopeSpec("server")
      .noBootstrapper()
      .module {
        http4k.optionalKey<Int>()

        http4k.server {
          val intKey = http4k.getKey<Int>()
          val app = http4k.initialiseRequestContext()
            .then(routes("/health" bind Method.GET to { println(intKey(it)); Response(OK) }))
          app.asServer(Undertow(8080))
        }
      }
      .build()
  }

  object TestScopeExecutor : ScopeExecutor<Unit> {
    override suspend fun execute(scopeDsl: ScopeDsl): Unit {
      coroutineScope {
        launch {
          ServerScopeExecutor.execute(scopeDsl)
          println("done with server")
        }

        launch {
          delay(2000)
          scopeDsl.get<Http4kServerStopper>().stop()
          println("stop")
        }
      }
    }
  }
}