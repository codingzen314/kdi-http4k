package tech.codingzen.kdi.http4k

import org.http4k.core.RequestContexts
import org.http4k.filter.ServerFilters
import org.http4k.lens.RequestContextLens
import tech.codingzen.kdi.Tag
import tech.codingzen.kdi.data_structure.Kdi
import tech.codingzen.kdi.data_structure.Scope
import tech.codingzen.kdi.dsl.ScopeDsl

val ScopeDsl.http4k get() = KdiHttp4kScopeDsl(this)

class KdiHttp4kScopeDsl(val scopeDsl: ScopeDsl) {
  suspend inline fun <reified T> getKey(tag: Tag = Kdi.defaultTag): RequestContextLens<T> =
    scopeDsl.get(RequestContextKeyTag<T>(tag))

  suspend fun initialiseRequestContext() =
    ServerFilters.InitialiseRequestContext(scopeDsl.get<RequestContexts>())
}