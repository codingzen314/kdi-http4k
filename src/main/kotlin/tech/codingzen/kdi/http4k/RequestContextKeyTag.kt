package tech.codingzen.kdi.http4k

import tech.codingzen.kdi.Tag

data class RequestContextKeyTag(val tag: Tag, val className: String) {
  companion object {
    inline operator fun <reified T> invoke(tag: Tag): RequestContextKeyTag {
      val className = T::class.qualifiedName ?: throw IllegalStateException("type must have a qualifiedName")
      return RequestContextKeyTag(tag, className)
    }
  }
}