package tech.codingzen.kdi.http4k

import org.http4k.core.Request
import org.http4k.core.RequestContext
import org.http4k.core.Store
import org.http4k.lens.LensExtractor
import org.http4k.lens.LensInjector
import org.http4k.lens.RequestContextKey
import org.http4k.lens.RequestContextLens
import java.util.*

interface IRequestContextKey<T> : LensInjector<T, Request>, LensExtractor<Request, T> {
  val key: RequestContextLens<T>

  override fun invoke(target: Request): T = key(target)

  override fun <R : Request> invoke(value: T, target: R): R = key(value, target)

  @JvmInline
  value class Impl<T>(override val key: RequestContextLens<T>) : IRequestContextKey<T>

  companion object {
    fun <T> requiredKey(store: Store<RequestContext>, name: String = UUID.randomUUID().toString()): IRequestContextKey<T> =
      Impl(RequestContextKey.required<T>(store, name))

    fun <T> optionalKey(store: Store<RequestContext>, name: String = UUID.randomUUID().toString()): IRequestContextKey<T?> =
      Impl(RequestContextKey.optional<T>(store, name))

    fun <T> defaultedKey(store: Store<RequestContext>, default: T, name: String = UUID.randomUUID().toString()): IRequestContextKey<T> =
      Impl(RequestContextKey.defaulted<T>(store, default, name))
  }
}