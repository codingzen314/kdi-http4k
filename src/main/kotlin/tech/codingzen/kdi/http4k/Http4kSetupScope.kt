package tech.codingzen.kdi.http4k

import kotlinx.coroutines.channels.Channel
import org.http4k.core.RequestContexts
import tech.codingzen.kdi.Tag
import tech.codingzen.kdi.data_structure.Kdi
import tech.codingzen.kdi.data_structure.KdiModule
import tech.codingzen.kdi.data_structure.KdiModulesFactory
import tech.codingzen.kdi.dsl.module
import tech.codingzen.kdi.dsl.modules
import tech.codingzen.kdi.spec.BaseSpec

object Http4kSetupScope : BaseSpec.Delegate {
  const val scopeId = "KdiHttp4k"

  override val scopeSpec = Kdi.scopeSpec(scopeId)
    .bootstrapper { Http4kSetupBootstrapper() }
    .build()
}

class Http4kSetupBootstrapper : KdiModulesFactory {
  companion object {
    val channelTag: Tag = "__http4k-wait-channel__"
  }

  override suspend fun create(): List<KdiModule> {
    val channel = Channel<Unit>(Channel.RENDEZVOUS)

    return modules {
      +module {
        single { RequestContexts() }
        instance(channel, channelTag)
        single { Http4kServerStopper(channel) }
      }
    }
  }
}