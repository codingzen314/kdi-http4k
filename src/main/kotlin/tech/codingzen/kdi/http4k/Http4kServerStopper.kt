package tech.codingzen.kdi.http4k

import kotlinx.coroutines.channels.Channel

class Http4kServerStopper(private val channel: Channel<Unit>) {
  fun stop() {
    channel.trySend(Unit)
  }
}