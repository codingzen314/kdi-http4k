package tech.codingzen.kdi.http4k

import kotlinx.coroutines.channels.Channel
import org.http4k.server.Http4kServer

class Http4kKdiServerStarter(
  private val waiter: Channel<Unit>,
  private val stopper: Http4kServerStopper,
  private val server: Http4kServer
) {
  suspend fun start() {
    Runtime.getRuntime().addShutdownHook(Thread { stopper.stop() })
    server.start()
    waiter.receive()
    server.stop()
  }
}