package tech.codingzen.kdi.http4k

import org.http4k.core.RequestContexts
import org.http4k.lens.RequestContextKey
import org.http4k.server.Http4kServer
import tech.codingzen.kdi.Provider
import tech.codingzen.kdi.Tag
import tech.codingzen.kdi.data_structure.Kdi
import tech.codingzen.kdi.dsl.module.ModuleDsl
import java.util.*

val ModuleDsl.http4k get() = KdiHttp4kModuleDsl(this)

class KdiHttp4kModuleDsl(val moduleDsl: ModuleDsl) {
  fun server(provider: Provider<Http4kServer>): Unit {
    moduleDsl.single(provider = provider)
  }

  inline fun <reified T> key(crossinline creator: (RequestContexts) -> T): Unit {
    moduleDsl.single { creator(get()) }
  }

  inline fun <reified T> requiredKey(name: String? = null, tag: Tag = Kdi.defaultTag): Unit {
    moduleDsl.single(RequestContextKeyTag<T>(tag)) {
      val contexts = get<RequestContexts>()
      name?.let { RequestContextKey.required<T>(contexts, it) } ?: RequestContextKey.required<T>(contexts)
    }
  }

  inline fun <reified T> optionalKey(name: String? = null, tag: Tag = Kdi.defaultTag): Unit {
    moduleDsl.single(RequestContextKeyTag<T>(tag)) {
      val contexts = get<RequestContexts>()
      name?.let { RequestContextKey.optional<T>(contexts, it) } ?: RequestContextKey.optional<T>(contexts)
    }
  }

  inline fun <reified T> defaultedKey(default: T, name: String? = null, tag: Tag = Kdi.defaultTag): Unit {
    moduleDsl.single(RequestContextKeyTag<T>(tag)) {
      val contexts = get<RequestContexts>()
      name?.let { RequestContextKey.defaulted<T>(contexts, default, it) } ?: RequestContextKey.defaulted(contexts, default)
    }
  }
}
