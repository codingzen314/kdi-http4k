package tech.codingzen.kdi.http4k

import kotlinx.coroutines.channels.Channel
import org.http4k.server.Http4kServer
import tech.codingzen.kdi.data_structure.ScopeExecutor
import tech.codingzen.kdi.dsl.ScopeDsl

object ServerScopeExecutor : ScopeExecutor<Unit> {
  override suspend fun execute(dsl: ScopeDsl): Unit {
    val channel = dsl.get<Channel<Unit>>(Http4kSetupBootstrapper.channelTag)
    val stopper = dsl.get<Http4kServerStopper>()
    val server = dsl.get<Http4kServer>()
    Http4kKdiServerStarter(channel, stopper, server).start()
  }
}